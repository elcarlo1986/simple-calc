const ErrorResponse = require('../utils/errorResponse');

exports.add = (req, res, next) => {
  let { a, b } = req.query;

  let result;
  if (isNaN(a + '') || isNaN(b + '')) {
    return next(new ErrorResponse('Input must be a number', 400));
  }
  a = parseFloat(a);
  b = parseFloat(b);

  result = Math.round((a + b) * 100) / 100;
  res.status(200).json({
    result,
    success: true,
  });
};

exports.sub = (req, res, next) => {
  let { a, b } = req.query;

  let result;
  if (isNaN(a + '') || isNaN(b + '')) {
    const error = new ErrorResponse('Input must be a number', 400);
    return next(error);
  }
  a = parseFloat(a);
  b = parseFloat(b);

  result = Math.round((a - b) * 100) / 100;
  res.status(200).json({
    result,
    success: true,
  });
};

exports.mul = (req, res, next) => {
  let { a, b } = req.query;

  let result;
  if (isNaN(a + '') || isNaN(b + '')) {
    const error = new ErrorResponse('Input must be a number', 400);
    return next(error);
  }
  a = parseFloat(a);
  b = parseFloat(b);

  result = Math.round(a * b * 100) / 100;
  res.status(200).json({
    result,
    success: true,
  });
};

exports.div = (req, res, next) => {
  let { a, b } = req.query;

  let result;
  if (isNaN(a + '') || isNaN(b + '')) {
    const error = new ErrorResponse('Input must be a number', 400);
    return next(error);
  }

  a = parseFloat(a);
  b = parseFloat(b);

  if (b === 0.0) {
    const error = new ErrorResponse('Denominator cannot be 0', 400);
    return next(error);
  }

  result = Math.round((a / b) * 100) / 100;
  res.status(200).json({
    result,
    success: true,
  });
};
