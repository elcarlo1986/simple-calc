const express = require('express');
const app = express();

const calculatorRoutes = require('../routes/calculator');

const errorHandler = require('../middleware/error');

app.use(express.json());



app.get('/', function (req, res) {
    res.status(200).json({
        msg: "Simple Calculator"
    });
});

app.use('/calculator', calculatorRoutes)

app.use(errorHandler);


module.exports = app;