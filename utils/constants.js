const ADDITION_PATH = '/calculator/add';

const SUBTRACTION_PATH = '/calculator/sub';

const MULTIPLICATION_PATH = '/calculator/mul';

const DIVISION_PATH = '/calculator/div';

module.exports = {
  ADDITION_PATH,
  SUBTRACTION_PATH,
  MULTIPLICATION_PATH,
  DIVISION_PATH,
};
