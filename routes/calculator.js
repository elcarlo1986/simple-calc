const express = require('express');
const router = express.Router();

const controller = require('../controllers/calculator');

router.get('/add', controller.add);
router.get('/sub', controller.sub);
router.get('/mul', controller.mul);
router.get('/div', controller.div);


module.exports = router;