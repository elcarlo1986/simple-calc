// Notification close button
const close = document.getElementById('close');
// Notification element
const notification = document.getElementById('notification');
// Display element
const display = document.getElementById('display');

// Numbers buttons
const key0 = document.getElementById('key0');
const key1 = document.getElementById('key1');
const key2 = document.getElementById('key2');
const key3 = document.getElementById('key3');
const key4 = document.getElementById('key4');
const key5 = document.getElementById('key5');
const key6 = document.getElementById('key6');
const key7 = document.getElementById('key7');
const key8 = document.getElementById('key8');
const key9 = document.getElementById('key9');

// Operations buttons
const addition = document.getElementById('addition');
const subtraction = document.getElementById('subtraction');
const multiplication = document.getElementById('multiplication');
const division = document.getElementById('division');

// Decimal point button
const decimal = document.getElementById('decimal');

// Equal button
const equal = document.getElementById('equal');

// Reset button
const reset = document.getElementById('reset');

// Variables
let previousResult = 0;

let currentResult = 0;

let currentDisplay = '';

let currentOperation = '';

let resolved = false;

// Auxiliar array of numbers
const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
// Auxiliar array of operations
const operations = ['+', '-', '*', '/'];

// Funtions

// Set current display value
function setCurrentDisplay(number) {
  currentDisplay += number;
}

// Set current result value
function setCurrentResult() {
  currentResult = Math.round(+currentDisplay * 100) / 100;
}

// Set display value
function setDisplay() {
  display.value = currentResult;
}

// Resolves the current operation
function resolveOperation() {
  switch (currentOperation) {
    case '+':
      previousResult += currentResult;
      break;
    case '-':
      previousResult -= currentResult;
      break;
    case '*':
      previousResult *= currentResult;
      break;
    case '/':
      if (currentResult !== 0) {
        previousResult /= currentResult;
      }
      break;
    default:
      break;
  }
}

// Set current operation
function setCurrentOperation(operation) {
  if (previousResult !== 0) {
    resolveOperation();
  } else {
    previousResult = currentResult;
  }
  currentResult = 0;
  currentDisplay = '';
  currentOperation = operation;
}

function calculateResult() {
  resolved = true;
  resolveOperation();
  currentResult = previousResult;
  setDisplay();
}

function addDecimalPoint() {
  if (!currentDisplay.includes('.')) {
    currentDisplay += '.';
  }
}

function resetValues() {
  resolved = false;
  currentResult = 0;
  previousResult = 0;
  currentOperation = '';
  currentDisplay = '';
  display.value = '';
}

// Event listeners
document.addEventListener('keypress', (e) => {
  if (numbers.includes(+e.key)) {
    if (resolved) {
      resetValues();
    }
    setCurrentDisplay(e.key);
    setCurrentResult();
    setDisplay();
  } else if (operations.includes(e.key)) {
    console.log('Operation');
    setCurrentOperation(e.key);
    setDisplay();
  } else if (e.key === '.') {
    console.log('Decimal');
    addDecimalPoint();
    setCurrentResult();
    setDisplay();
  } else if (e.key === 'Enter') {
    console.log('Equal');
    calculateResult();
  }
});

// Close notification
close.addEventListener('click', () => {
  notification.style.display = 'none';
});

// Click on numbers
key0.addEventListener('click', () => {});
key1.addEventListener('click', () => {});
key2.addEventListener('click', () => {});
key3.addEventListener('click', () => {});
key4.addEventListener('click', () => {});
key5.addEventListener('click', () => {});
key6.addEventListener('click', () => {});
key7.addEventListener('click', () => {});
key8.addEventListener('click', () => {});
key9.addEventListener('click', () => {});

// Click on operation
addition.addEventListener('click', () => {});
subtraction.addEventListener('click', () => {});
multiplication.addEventListener('click', () => {});
division.addEventListener('click', () => {});

// Click on decimal point
decimal.addEventListener('click', () => {});

// Click on equal
equal.addEventListener('click', () => {});

//Reset button
reset.addEventListener('click', () => {
  resetValues();
});
