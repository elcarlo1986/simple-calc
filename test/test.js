const request = require('supertest');
const app = require('../app/index');

describe('Home Endpoint', () => {
    it('should return an object with msg property', async () => {
        const res = await request(app).get('/');
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('msg');
    });
});