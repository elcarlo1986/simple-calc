const request = require('supertest');
const app = require('../app/index');

const {
  ADDITION_PATH,
  SUBTRACTION_PATH,
  MULTIPLICATION_PATH,
  DIVISION_PATH,
} = require('../utils/constants');

const invalidNumber = (done, route) => {
  request(app)
    .get(route)
    .query({
      a: 'a',
      b: 'b',
    })
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(400)
    .end((err, res) => {
      expect(res.body).toHaveProperty('error', 'Input must be a number');
      expect(res.body).toHaveProperty('success', false);
      done();
    });
};

describe('Addition Endpoint', () => {
  it('Tests the addition endpoint, with a valid query', async () => {
    const res = await request(app)
      .get(ADDITION_PATH)
      .query({
        a: 1,
        b: 2,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(res.body).toHaveProperty('result');
    expect(res.body).toHaveProperty('success');
    expect(res.body.result).toBeCloseTo(3);
    expect(res.body.success).toBe(true);
  });

  it('Tests the addition endpoint, with a invalid query', (done) => {
    invalidNumber(done, ADDITION_PATH);
  });
});

describe('Subtraction Endpoint', () => {
  it('Tests the subtraction endpoint, with a valid query', async () => {
    const res = await request(app)
      .get(SUBTRACTION_PATH)
      .query({
        a: 1,
        b: 2,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(res.body).toHaveProperty('result');
    expect(res.body).toHaveProperty('success');
    expect(res.body.result).toBeCloseTo(-1);
    expect(res.body.success).toBe(true);
  });

  it('Tests the subtraction endpoint, with a invalid query', (done) => {
    invalidNumber(done, SUBTRACTION_PATH);
  });
});

describe('Multiplication Endpoint', () => {
  it('Tests the multiplication endpoint, with a valid query', async () => {
    const res = await request(app)
      .get(MULTIPLICATION_PATH)
      .query({
        a: 3,
        b: 2,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(res.body).toHaveProperty('result');
    expect(res.body).toHaveProperty('success');
    expect(res.body.result).toBeCloseTo(6);
    expect(res.body.success).toBe(true);
  });

  it('Tests the multiplication endpoint, with a invalid query', (done) => {
    invalidNumber(done, MULTIPLICATION_PATH);
  });
});

describe('Division Endpoint', () => {
  it('Tests the division endpoint, with a valid query', async () => {
    const res = await request(app)
      .get(DIVISION_PATH)
      .query({
        a: 1,
        b: 2,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(res.body).toHaveProperty('result');
    expect(res.body).toHaveProperty('success');
    expect(res.body.result).toBeCloseTo(0.5);
    expect(res.body.success).toBe(true);
  });

  it('Tests the division endpoint, with a invalid query', (done) => {
    invalidNumber(done, DIVISION_PATH);
  });

  it('Tests the division endpoint, with denominator zero', (done) => {
    request(app)
      .get(DIVISION_PATH)
      .query({
        a: 1,
        b: 0,
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400)
      .end((err, res) => {
        expect(res.body).toHaveProperty('error', 'Denominator cannot be 0');
        expect(res.body).toHaveProperty('success', false);
        done();
      });
  });
});
