const errorHandler = require('../middleware/error');

describe('middleware.ErrorHandler', () => {
  /**
   * Mocked Express Request object.
   */
  let req;

  /**
   * Mocked Express Response object.
   */
  let res;

  /**
   * Mocked Express Next function.
   */
  const next = jest.fn();

  /**
   * Reset the `req` and `res` object before each test is ran.
   */
  beforeEach(() => {
    req = {
      params: {},
      body: {},
    };

    res = {
      data: null,
      code: null,
      body: {},
      status(status) {
        this.code = status;
        return this;
      },
      send(payload) {
        this.data = payload;
      },
      json({ success, error }) {
        this.body.success = success;
        this.body.error = error;
      },
    };

    next.mockClear();
  });

  test('should handle error', () => {
    errorHandler(new Error(), req, res, next);

    expect(res.code).toBeDefined();
    expect(res.code).toBe(500);

    expect(res.body.error).toBeDefined();
    expect(res.body.error).toBe('Server Error');

    expect(res.body.success).toBeDefined();
    expect(res.body.success).toBe(false);
  });
});
